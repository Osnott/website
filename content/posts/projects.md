---
title: "Projects"
date: 2020-02-22T14:46:35-07:00
draft: false
toc: false
images:
tags: 
  - FRC
  - programming
---

## Projects that I am currently working on

### Infinite Recharge code
- [Pathfinding with Ramsete Controller](https://github.com/Team6479/ChinampaLibrary/tree/master/wpi/src/main/java/com/team6479/lib)
- General subsystems

### Personal Projects
- My desktop rice
- Improving this website
- [MCLayer](https://gitlab.com/Osnott/mclayer)