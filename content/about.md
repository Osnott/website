---
title: "About"
date: 2020-02-21T16:31:51-07:00
draft: false
---

### Code

- I'm currently experinced in Java and Python and a member of FRC Team 6479.

### Art

- I use Adobe Illustrator, Premier, Photoshop, and After Effects.
- Currently a member of the Corona del Sol PIT and the Corona del Sol Percussion group.
- Also I compose music and have an interest in sound synthesis/engineering.